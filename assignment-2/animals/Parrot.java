package animals;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Scanner;
import cage.IndoorCage;

public class Parrot extends Animals{
	private static String name;
	private static int length;
	private static ArrayList<IndoorCage> cageParrot = new ArrayList<IndoorCage>();

	public Parrot(String name, int length){
		super(name, length);
	}
	public static void createParrotCage(){
		ArrayList<Parrot> parrotList = new ArrayList<Parrot>();

		Scanner scan = new Scanner(System.in);
		System.out.println("parrot: ");
		int totalAnimals = scan.nextInt();
		scan.nextLine();

		if (totalAnimals > 0){
			System.out.println("Provide the information of parrot(s):");
			String animalInfo = scan.nextLine();
			String[] splitParrotInfo = animalInfo.split(","); 

			for (String parrotInfo : splitParrotInfo){
				String[] parrotData = parrotInfo.split("[|]");
				Parrot kakatua = new Parrot(parrotData[0], Integer.parseInt(parrotData[1]));
				parrotList.add(kakatua);
			}
			for (Parrot kakatua : parrotList){
				IndoorCage cage = new IndoorCage(kakatua);
				cageParrot.add(cage);
			}
		}
	}
	public static ArrayList<IndoorCage> getParrotCage(){
		return cageParrot;
	}
	public static void visitParrot(){
		Scanner scan = new Scanner(System.in);
		System.out.println("Mention the name of parrot you want to visit: ");
		String inputName = scan.next();
		String sound = "";

		for (IndoorCage cage : cageParrot){
			if (cage.getHewan().getName().equals(inputName)){
				System.out.printf("%nYou are visiting %s (parrot) now, what would you like to do?%n", inputName);
				System.out.println("1: Order to fly 2: Do conversation");
				int inputNum = scan.nextInt();
				if (inputNum == 1){
					System.out.println("Parrot " + inputName + " flies!");
					sound = "FLYYYY.....";
					System.out.println(inputName + " makes a voice: " + sound);
				}else if (inputNum == 2){
					System.out.println("You say: ");
					String suara = scan.next();
					sound = suara;	
					System.out.println(inputName + " says: " + sound);
				}else{
					sound = "HM?";
					System.out.println(inputName + " says: " + sound);
				}
				System.out.println("Back to the office!\n");
				break;
			}else{
				System.out.println("There is no parrot with that name! Back to the office!\n");
				break;
			}
		}
	}
	public static void parrotTotal(){
		System.out.println("parrot:" + String.valueOf(cageParrot.size()));
	}
}