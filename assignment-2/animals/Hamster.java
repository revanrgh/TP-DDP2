package animals;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Scanner;
import cage.IndoorCage;

public class Hamster extends Animals{
	private static String name;
	private static int length;
	private static ArrayList<IndoorCage> cageHamster = new ArrayList<IndoorCage>();

	public Hamster(String name, int length){
		super(name, length);
	}
	public static void createHamsterCage(){
		ArrayList<Hamster> hamsterList = new ArrayList<Hamster>();

		Scanner scan = new Scanner(System.in);
		System.out.println("hamster: ");
		int totalAnimals = scan.nextInt();
		scan.nextLine();

		if (totalAnimals > 0){
			System.out.println("Provide the information of hamster(s):");
			String animalInfo = scan.nextLine();
			String[] splitHamsterInfo = animalInfo.split(","); //name|90,nama|80

			for (String hamsterInfo : splitHamsterInfo){
				String[] hamsterData = hamsterInfo.split("[|]");
				Hamster hamsters = new Hamster(hamsterData[0], Integer.parseInt(hamsterData[1]));
				hamsterList.add(hamsters);
			}
			for (Hamster hamsters : hamsterList){
				IndoorCage cage = new IndoorCage(hamsters);
				cageHamster.add(cage);
			}
		}
	}
	public static ArrayList<IndoorCage> getHamsterCage(){
		return cageHamster;
	}
	public static void visitHamster(){
		Scanner scan = new Scanner(System.in);
		System.out.println("Mention the name of hamster you want to visit: ");
		String inputName = scan.next();
		String sound = "";

		for (IndoorCage cage : cageHamster){
			if (cage.getHewan().getName().equals(inputName)){
				System.out.printf("%nYou are visiting %s (hamster) now, what would you like to do?%n", inputName);
				System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
				int inputNum = scan.nextInt();
				if (inputNum == 1){
					sound = "ngkkrit.. ngkkrrriiit";
				}else if (inputNum == 2){
					sound = "trrr.... trrr...";
				}else{
					System.out.println("You do nothing...\n");
					break;
				}
				System.out.println(inputName + " makes a voice: " + sound);
				System.out.println("Back to the office!\n");
				break;
			}else{
				System.out.println("There is no hamster with that name! Back to the office!\n");
				break;
			}
		}
	}
	public static void hamsterTotal(){
		System.out.println("hamster:" + String.valueOf(cageHamster.size()));
	}
}