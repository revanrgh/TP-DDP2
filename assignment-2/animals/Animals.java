package animals;

import java.util.*;

public class Animals{
	protected String name;
	protected int length;

	public Animals(String name, int length){
		this.name = name;
		this.length = length;
	}
	public String getName(){
		return this.name;
	}
	public int getLength(){
		return this.length;
	}
}