package animals;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Scanner;
import cage.IndoorCage;

public class Cat extends Animals{
	private static String name;
	private static int length;
	private static ArrayList<IndoorCage> cageCat = new ArrayList<IndoorCage>();

	public Cat(String name, int length){
		super(name, length);
	}
	public static void createCatCage(){
		ArrayList<Cat> catList = new ArrayList<Cat>();

		Scanner scan = new Scanner(System.in);
		System.out.println("cat: ");
		int totalAnimals = scan.nextInt();
		scan.nextLine();

		if (totalAnimals > 0){
			System.out.println("Provide the information of cat(s):");
			String animalInfo = scan.nextLine();
			String[] splitCatInfo = animalInfo.split(","); 

			for (String catInfo : splitCatInfo){
				String[] catData = catInfo.split("[|]");
				Cat kucing = new Cat(catData[0], Integer.parseInt(catData[1]));
				catList.add(kucing);
			}
			for (Cat kucing : catList){
				IndoorCage cage = new IndoorCage(kucing);
				cageCat.add(cage);
			}
		}
	}

	public static ArrayList<IndoorCage> getCatCage(){
		return cageCat;
	}
	public static void visitCat(){
		Scanner scan = new Scanner(System.in);
		System.out.println("Mention the name of cat you want to visit: ");
		String inputName = scan.next();
		String sound = "";

		for (IndoorCage cage : cageCat){
			if (cage.getHewan().getName().equals(inputName)){
				System.out.printf("%nYou are visiting %s (cat) now, what would you like to do?%n", inputName);
				System.out.println("1: Brush the fur 2: Cuddle");
				int inputNum = scan.nextInt();
				if (inputNum == 1){
					sound = "Nyaaan...";
				}else if (inputNum == 2){
					sound = "Purrr..";
				}else{
					System.out.println("You do nothing...\n");
					break;
				}
				System.out.println(inputName + " makes a voice: " + sound);
				System.out.println("Back to the office!\n");
				break;
			}else{
				System.out.println("There is no cat with that name! Back to the office!\n");
				break;
			}
		}
	}
	public static void catTotal(){
		System.out.println("cat:" + String.valueOf(cageCat.size()));
	}
}