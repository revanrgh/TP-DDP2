package animals;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Scanner;
import cage.OutdoorCage;

public class Eagle extends Animals{
	private static String name;
	private static int length;
	private static ArrayList<OutdoorCage> cageEagle = new ArrayList<OutdoorCage>();

	public Eagle(String name, int length){
		super(name, length);
	}
	public static void createEagleCage(){
		ArrayList<Eagle> eagleList = new ArrayList<Eagle>();

		Scanner scan = new Scanner(System.in);
		System.out.println("eagle: ");
		int totalAnimals = scan.nextInt();
		scan.nextLine();

		if (totalAnimals > 0){
			System.out.println("Provide the information of eagle(s):");
			String animalInfo = scan.nextLine();
			String[] splitEagleInfo = animalInfo.split(","); 

			for (String eagleInfo : splitEagleInfo){
				String[] eagleData = eagleInfo.split("[|]");
				Eagle elang = new Eagle(eagleData[0], Integer.parseInt(eagleData[1]));
				eagleList.add(elang);
			}
			for (Eagle elang : eagleList){
				OutdoorCage cage = new OutdoorCage(elang);
				cageEagle.add(cage);
			}
		}		
	}
	public static ArrayList<OutdoorCage> getEagleCage(){
		System.out.print(cageEagle.size());
		return cageEagle;
	}
	public static void visitEagle(){
		Scanner scan = new Scanner(System.in);
		System.out.println("Mention the name of eagle you want to visit: ");
		String inputName = scan.next();
		String sound = "";

		for (OutdoorCage cage : cageEagle){
			if (cage.getHewan().getName().equals(inputName)){
				System.out.printf("%nYou are visiting %s (eagle) now, what would you like to do?%n", inputName);
				System.out.println("1: Order to fly");
				int inputNum = scan.nextInt();
				if (inputNum == 1){
					sound = "kwaakk...";
					System.out.print("You hurt!");
					System.out.println(inputName + " makes a voice: " + sound);
				}else{
					System.out.println("You do nothing...\n");
				}
				System.out.println("Back to the office!\n");
				break;
			}else{
				System.out.println("There is no eagle with that name! Back to the office!\n");
				break;
			}
		}
	}
	public static void eagleTotal(){
		System.out.println("eagle:" + String.valueOf(cageEagle.size()));
	}
}