package animals;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Scanner;
import cage.OutdoorCage;

public class Lion extends Animals{
	private static String name;
	private static int length;
	private static ArrayList<OutdoorCage> cageLion = new ArrayList<OutdoorCage>();

	public Lion(String name, int length){
		super(name, length);
	}
	public static void createLionCage(){
		ArrayList<Lion> lionList = new ArrayList<Lion>();

		Scanner scan = new Scanner(System.in);
		System.out.println("lion: ");
		int totalAnimals = scan.nextInt();
		scan.nextLine();

		if (totalAnimals > 0){
			System.out.println("Provide the information of lion(s):");
			String animalInfo = scan.nextLine();
			String[] splitLionInfo = animalInfo.split(","); 

			for (String lionInfo : splitLionInfo){
				String[] lionData = lionInfo.split("[|]");
				Lion singa = new Lion(lionData[0], Integer.parseInt(lionData[1]));
				lionList.add(singa);
			}
			for (Lion singa : lionList){
				OutdoorCage cage = new OutdoorCage(singa);
				cageLion.add(cage);
			}
		}	
	}
	public static ArrayList<OutdoorCage> getLionCage(){
		return cageLion;
	}
	public static void visitLion(){
		Scanner scan = new Scanner(System.in);
		System.out.println("Mention the name of lion you want to visit: ");
		String inputName = scan.next();
		String sound = "";

		for (OutdoorCage cage : cageLion){
			if (cage.getHewan().getName().equals(inputName)){
				System.out.printf("%nYou are visiting %s (lion) now, what would you like to do?%n", inputName);
				System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
				int inputNum = scan.nextInt();
				if (inputNum == 1){
					System.out.println("Lion is hunting..");
					sound = "err...!";
				}else if (inputNum == 2){
					System.out.println("Clean the lion’s mane..");
					sound = "Hauhhmm!";
				}else if (inputNum == 3){
					sound = "HAUHHMM!";
				}else{
					System.out.println("You do nothing...\n");
					break;
				}
				System.out.println(inputName + " makes a voice: " + sound);
				System.out.println("Back to the office!\n");
				break;
			}else{
				System.out.println("There is no lion with that name! Back to the office!\n");
				break;
			}
		}
	}
	public static void lionTotal(){
		System.out.println("lion:" + String.valueOf(cageLion.size()));
	}
}