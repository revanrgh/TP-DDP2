package arrangement;

import java.util.ArrayList;
import java.util.Arrays;
import animals.*;
import cage.*;
import java.util.*;


public class CageArrangement{

    private static IndoorCage[][] listOfIndoorLevelCages = new IndoorCage[3][];
    private static OutdoorCage[][] listOfOutdoorLevelCages = new OutdoorCage[3][];

	public static void makeLevelIndoorCage(ArrayList<IndoorCage> cageList){
		int cageLength = cageList.size();
		int size = cageLength/3;
		int remainder = cageLength%3;

        if(cageList.size() > 0){
            if(size == 0){
                size = 1;
            }
            if(cageLength <= 3){
                remainder = 0;
            }
            listOfIndoorLevelCages[2] = new IndoorCage[size];
            listOfIndoorLevelCages[1] = new IndoorCage[size];
            listOfIndoorLevelCages[0] = new IndoorCage[size + remainder];

            for(int x = 0; x < size; x++){
                listOfIndoorLevelCages[2][x] = cageList.get(x);
            }

            if(cageList.size() > 1){
                for(int x = 0; x < size; x++){
                    listOfIndoorLevelCages[1][x] = cageList.get(x + size);
                }
            }

            if(cageList.size() > 2){
                for(int x = 0; x < size + remainder; x++){
                    listOfIndoorLevelCages[0][x] = cageList.get(x + size*2);
                }
            }
            printCage(listOfIndoorLevelCages);
            rearrangeCages(listOfIndoorLevelCages);
        }
	}
	public static void makeLevelOutdoorCage(ArrayList<OutdoorCage> cageList){
		int cageLength = cageList.size();
		int size = cageLength/3;
		int remainder = cageLength%3;

        if(cageList.size() > 0){
            if(size == 0){
                size = 1;
            }
            if(cageLength <= 3){
                remainder = 0;
            }
            listOfOutdoorLevelCages[2] = new OutdoorCage[size];
            listOfOutdoorLevelCages[1] = new OutdoorCage[size];
            listOfOutdoorLevelCages[0] = new OutdoorCage[size + remainder];

            for(int x = 0; x < size; x++){
                listOfOutdoorLevelCages[2][x] = cageList.get(x);
            }

            if(cageList.size() > 1){
                for(int x = 0; x < size; x++){
                    listOfOutdoorLevelCages[1][x] = cageList.get(x + size);
                }
            }

            if(cageList.size() > 2){
                for(int x = 0; x < size + remainder; x++){
                    listOfOutdoorLevelCages[0][x] = cageList.get(x + size*2);
                }
            }
            printCage(listOfOutdoorLevelCages);
            rearrangeCages(listOfOutdoorLevelCages);
        }
	}
    public static void rearrangeCages(IndoorCage[][] arrange){
        for(int x = 0; x < arrange.length; x++){
            if(arrange[x].length > 1){
                for(int y = 0; y < arrange.length/2; y++){
                    IndoorCage indoorcage = arrange[x][y];
                    arrange[x][y] = arrange[x][arrange.length-y-1];
                    arrange[x][arrange.length-y-1] = indoorcage;
                }
            }
        }
        IndoorCage[][] indoorCages = {arrange[1], arrange[2], arrange[0]};
        System.out.println("After rearrangement..");
        printCage(indoorCages);
    }
    public static void rearrangeCages(OutdoorCage[][] arrange){
        for(int x = 0; x < arrange.length; x++){
            if(arrange[x].length > 1){
                for(int y = 0; y < arrange.length/2; y++){
                    OutdoorCage outdoorcage = arrange[x][y];
                    arrange[x][y] = arrange[x][arrange.length-y-1];
                    arrange[x][arrange.length-y-1] = outdoorcage;
                }
            }
        }
        OutdoorCage[][] outdoorCages = {arrange[1], arrange[2], arrange[0]};
        System.out.println("After rearrangement..");
        printCage(outdoorCages);
    }
    
    public static void printCage(IndoorCage[][] kandang){
        System.out.println("location: indoor");
        String hasil = "";
        for(int x = 0; x< kandang.length; x++){
            hasil += ("level " + (kandang.length-x) + ": ");
            for(int y = 0; y < kandang[x].length; y++){
                if(kandang[x][y] != null){
                    hasil += (kandang[x][y].getHewan().getName() + " (" + kandang[x][y].getHewan().getLength() + " - " + kandang[x][y].getTipe() + "), ");
                }
            }
            if(x != 2){
                hasil += "\n";
            }
        }
        System.out.println(hasil + "\n");
    }

    public static void printCage(OutdoorCage[][] kandang){
        System.out.println("location: outdoor");
        String hasil = "";
        for(int x = 0; x< kandang.length; x++){
            hasil += ("level " + (kandang.length-x) + ": ");
            for(int y = 0; y < kandang[x].length; y++){
                if(kandang[x][y] != null){
                    hasil += (kandang[x][y].getHewan().getName() + " (" + kandang[x][y].getHewan().getLength() + " - " + kandang[x][y].getTipe() + ")");
                }
            }
            if(x != 2){
                hasil += "\n";
            }
        }
        System.out.println(hasil + "\n");
    }

}