package cage;

import animals.Animals;
import java.util.*;

public class OutdoorCage{

    private Animals hewan;
    private String tipe;

    public OutdoorCage(Animals hewan){
        this.hewan = hewan;
        String tipe = "";
        if(hewan.getLength() < 75){
            tipe = "A";
        }else if(75 <= hewan.getLength() && hewan.getLength() <= 90){
            tipe = "B";
        }else if(hewan.getLength() > 90){
            tipe = "C";
        }
        this.tipe = tipe;
    }   
    public Animals getHewan(){
        return hewan;
    }
    public String getTipe(){
        return tipe;
    }
} 