package cage;

import animals.Animals;
import java.util.*;

public class IndoorCage{

    private Animals hewan;
    private String tipe;

    public IndoorCage(Animals hewan){
        this.hewan = hewan;
        String tipe = "";
        if(hewan.getLength() < 45){
            tipe = "A";
        }else if(45 <= hewan.getLength() && hewan.getLength() <= 60){
            tipe = "B";
        }else if(hewan.getLength() > 60){
            tipe = "C";
        }
        this.tipe = tipe;
    }   
    public Animals getHewan(){
        return hewan;
    }
    public String getTipe(){
        return tipe;
    }
} 