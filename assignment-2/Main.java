import java.util.Scanner;
import java.util.ArrayList;
import animals.Animals;
import animals.Cat;
import animals.Lion;
import animals.Eagle;
import animals.Parrot;
import animals.Hamster;
import cage.OutdoorCage;
import cage.IndoorCage;
import arrangement.CageArrangement;

public class Main{

	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);

		System.out.println("Welcome to Javari Park!");
		System.out.println("scan the number of animals");

		Cat.createCatCage();
		Lion.createLionCage();
		Eagle.createEagleCage();
		Parrot.createParrotCage();
		Hamster.createHamsterCage();

		System.out.println("Animals has been successfully recorded!");
		System.out.println("=============================================");
		
		CageArrangement.makeLevelIndoorCage(Cat.getCatCage());
		CageArrangement.makeLevelOutdoorCage(Lion.getLionCage());
		CageArrangement.makeLevelOutdoorCage(Eagle.getEagleCage());
		CageArrangement.makeLevelIndoorCage(Parrot.getParrotCage());
		CageArrangement.makeLevelIndoorCage(Hamster.getHamsterCage());

		System.out.println("\nANIMALS NUMBER:");

        Cat.catTotal();
        Lion.lionTotal();
        Eagle.eagleTotal();
        Parrot.parrotTotal();
        Hamster.hamsterTotal();	

        System.out.println("\n=============================================");
        while(true){
            System.out.println("Which animal you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)");
            int input = scan.nextInt();
            if(input == 1){
                Cat.visitCat();
            }else if(input == 2){
                Eagle.visitEagle();
            }else if(input == 3){
                Hamster.visitHamster();
            }else if(input == 4){
                Parrot.visitParrot();
            }else if(input == 5){
                Lion.visitLion();
            }else if(input == 99){
                System.exit(0);
            }else{
                System.out.println("Please enter a valid option!");
            }

        }

	}
}