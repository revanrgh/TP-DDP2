package javari.park;

import java.util.ArrayList;
import java.util.Arrays;

public class CircleOfFire extends Attractions{
    
	private static String[] validAnimals = {"Lion", "Whale", "Eagle"};
	private ArrayList<String> listAnimals = new ArrayList<String>();

	public CircleOfFire(String name, String type){
		super(name, type);
	}

	public static boolean checkValid(String type){
		for (String cekHewan : validAnimals){
			if (type.equals(cekHewan)){
				return true;
			}
		} return false;
	}
}