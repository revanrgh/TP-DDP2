// Assignment 3
// Nama		: Revan Ragha Andhito
// Kelas	: D
// NPM		: 1706039420

package javari.park;

import java.util.ArrayList;
import java.util.List;
import javari.animal.*;
import javari.park.*;

public class Attractions implements SelectedAttraction{

    private String name;
    private String type;

    private ArrayList<Animal> performers = new ArrayList<Animal>();
    private static ArrayList<Attractions> listAttractions = new ArrayList<Attractions>();
    private ArrayList<Attractions> attractions = new ArrayList<Attractions>();

    private static String[] validCategories = {"Explore the Mammals", "Reptillian Kingdom", "World of Aves"};
    private static String[] validAttractions = {"Circles of Fires", "Counting Masters", "Dancing Animals", "Passionate Coders"};
    
    public Attractions(){
    }

    public Attractions(String name, String type) {
        this.name = name;
        this.type = type;
        listAttractions.add(this);
    }

    public String getName() {
        return name;
    }
    public String getType() {
        return type;
    }
    public List<Animal> getPerformers() {
        return performers;
    }
    public static String[] getValidCategories(){
        return validCategories;
    }
    public static String[] getValidAttractions(){
        return validAttractions;
    }
    public static ArrayList<Attractions> getListAttractions() {
        return listAttractions;
    }
    public boolean addPerformer(Animal performer) {
        if (performer.isShowable()) {
            performers.add(performer);
            return true;
        }
        return false;
    }
    public ArrayList<Attractions> findAnimal(String animal){
        for (Attractions atraksi : listAttractions){
            if (atraksi.getType().equalsIgnoreCase(animal)){
                attractions.add(atraksi);
                break;
            }
        }
        return attractions;
    }
}