package javari.park;

import java.util.ArrayList;
import java.util.Arrays;

public class PassionateCoders extends Attractions{
    
    private static String[] validAnimals = {"Hamster", "Cat", "Snake"};
    private ArrayList<String> listAnimals = new ArrayList<String>();

    public PassionateCoders(String name, String type){
        super(name, type);
    }

    public static boolean checkValid(String type){
        for (String cekHewan : validAnimals){
            if (type.equals(cekHewan)){
                return true;
            }
        } return false;
    }
}