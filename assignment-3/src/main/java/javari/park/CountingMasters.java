package javari.park;

import java.util.ArrayList;
import java.util.Arrays;

public class CountingMasters extends Attractions{
    
    private static String[] validAnimals = {"Hamster", "Whale", "Parrot"};
    private ArrayList<String> listAnimals = new ArrayList<String>();

    public CountingMasters(String name, String type){
        super(name, type);
    }

    public static boolean checkValid(String type){
        for (String cekHewan : validAnimals){
            if (type.equals(cekHewan)){
                return true;
            }
        } return false;
    }
}