package javari.animal;

import javari.animal.*;
import java.util.Scanner;
import java.util.Arrays;

public class Reptiles extends Animal{
    
    private static String[] reptilia = {"Snake"};
    private boolean isTame = false;

    public Reptiles(Integer id, String type, String name, Gender gender, double length,
                  double weight, String tamed, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if(tamed.equals("tamed")){
            isTame = true;
        }
    }

    public boolean specificCondition(){
        return !(isTame);
    }

    public static String[] getReptilia(){
        return reptilia;
    }
}