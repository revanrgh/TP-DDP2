package javari.animal;

import javari.animal.*;
import java.util.Scanner;
import java.util.Arrays;

public class Aves extends Animal{

	private static String[] aves = {"Parrot", "Eagle"};
	private boolean isLayingEgg = false;

	public Aves(Integer id, String type, String name, Gender gender, double length,
                double weight, String layeggs, Condition condition){
		super(id, type, name, gender, length, weight, condition);
		if(layeggs.equalsIgnoreCase("laying eggs")){
			this.isLayingEgg = true;
		}
	}
    public boolean specificCondition(){
        return !(isLayingEgg);
    }
    public static String[] getAves(){
        return aves;
    }
}