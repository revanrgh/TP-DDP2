package javari.animal;

import javari.animal.*;
import java.util.Scanner;
import java.util.Arrays;

public class Mammals extends Animal{
    
    private static String[] mamalia = {"Cat", "Whale", "Hamster", "Lion"};
    private boolean isPregnant = false;

    public Mammals(Integer id, String type, String name, Gender gender, double length,
                   double weight, String pregnant, Condition condition){
        super(id, type, name, gender, length, weight, condition);
        if (pregnant.equalsIgnoreCase("pregnant")){
            this.isPregnant = true;
        }
    }
    public boolean specificCondition(){
        return !(isPregnant) && ((!getType().equals("Lion")) || (getGender().equals(Gender.MALE)));
    }
    public static String[] getMamalia(){
        return mamalia;
    }

}