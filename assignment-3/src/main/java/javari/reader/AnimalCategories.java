package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javari.animal.*;
import javari.park.*;
import java.util.Set;
import java.util.LinkedHashSet;

public class AnimalCategories extends CsvReader{
    
    public AnimalCategories(Path file) throws IOException{
        super(file);
    }

    public ArrayList<String> category = new ArrayList<String>();

    public long countValidRecords(){
        Set<String> valid = new LinkedHashSet<>();

        for(int x = 0; x < lines.size(); x++){
            String input = lines.get(x);
            String[] inputSplit = input.split(",");
            String tipeHewan = inputSplit[0];
            String kategoriHewan = inputSplit[1]; 
            String section = inputSplit[2];

            // Mengecek apakah kategori hewan  termasuk dalam list valid atraksi dan list hewan
            if (kategoriHewan.equals("mammals")){
                if(Arrays.asList(Attractions.getValidCategories()).contains(section) && 
                    Arrays.asList(Mammals.getMamalia()).contains(tipeHewan)){
                    valid.add(section);
                }
            } else if (kategoriHewan.equals("aves")){
                if(Arrays.asList(Attractions.getValidCategories()).contains(section) && 
                    Arrays.asList(Aves.getAves()).contains(tipeHewan)){
                    valid.add(section);
                }
            } else { // Reptiles
                if(Arrays.asList(Attractions.getValidCategories()).contains(section) && 
                    Arrays.asList(Reptiles.getReptilia()).contains(tipeHewan)){
                    valid.add(section);
                }
            }
        } return (long)valid.size();
    }

    public long countInvalidRecords(){
        long invalid = 0;

        // Mengecek jika ada input yang tidak sesuai atau ada yang tidak valid
        for(int x = 0; x < lines.size(); x++){
            String input = lines.get(x);
            String[] inputSplit = input.split(",");
            String tipeHewan = inputSplit[0];
            String kategoriHewan = inputSplit[1]; 
            String section = inputSplit[2];

            if (kategoriHewan.equals("mammals")){
                if(!(Arrays.asList(Attractions.getValidCategories()).contains(section)) 
                    && (!(Arrays.asList(Mammals.getMamalia()).contains(tipeHewan)))){
                    invalid += 1;
                }
            } else if (kategoriHewan.equals("aves")){
                if(!(Arrays.asList(Attractions.getValidCategories()).contains(section)) 
                    && (!(Arrays.asList(Aves.getAves()).contains(tipeHewan)))){
                    invalid += 1;
                }
            } else { // Reptiles
                if(!(Arrays.asList(Attractions.getValidCategories()).contains(section)) 
                    && (!(Arrays.asList(Reptiles.getReptilia()).contains(tipeHewan)))){
                    invalid += 1;
                }
            }
        } return invalid;
    }

}