package javari.reader;

import javari.park.*;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.LinkedHashSet;
import java.util.Set;

public class AnimalAttractions extends CsvReader{

    public AnimalAttractions(Path file) throws IOException{
        super(file);
    }
    public long countValidRecords(){
        Set<String> valid = new LinkedHashSet<>();

        for(int x = 0; x < lines.size(); x++){
            String input = lines.get(x);
            String[] inputSplit = input.split(",");
            String tipeHewan = inputSplit[0];
            String tipeAtraksi = inputSplit[1];

            if(Arrays.asList(Attractions.getValidAttractions()).contains(tipeAtraksi)){
                if (tipeAtraksi.equals("Circle of Fire")){
                    if(CircleOfFire.checkValid(tipeHewan)){
                        new Attractions("Circles of Fires", tipeHewan);
                        valid.add(tipeAtraksi);
                    }
                } else if (tipeAtraksi.equals("Counting Masters")){
                    if(CountingMasters.checkValid(tipeHewan)){
                        new Attractions("Counting Masters", tipeHewan);
                        valid.add(tipeAtraksi);
                    }
                } else if (tipeAtraksi.equals("Dancing Animals")){
                    if(DancingAnimals.checkValid(tipeHewan)){
                        new Attractions("Dancing Animals", tipeHewan);
                        valid.add(tipeAtraksi);
                    }
                } else { // Passionate Coders
                    if(PassionateCoders.checkValid(tipeHewan)){
                        new Attractions("Passionate Coders", tipeHewan);
                        valid.add(tipeAtraksi);
                    }
                }
            }
        }
        return (long)valid.size();
    }

    public long countInvalidRecords(){
        long invalid = 0;
        for(int x = 0; x < lines.size(); x++){
            String input = lines.get(x);
            String[] inputSplit = input.split(",");
            String tipeHewan = inputSplit[0];
            String tipeAtraksi = inputSplit[1];
            String section = inputSplit[2];

            if (Arrays.asList(Attractions.getValidAttractions()).contains(tipeAtraksi)){
                if (tipeAtraksi.equals("Circle of Fire")){
                    if (!CircleOfFire.checkValid(tipeHewan)){
                        invalid += 1;
                    } else{
                        continue;
                    }
                } else if (tipeAtraksi.equals("Counting Masters")){
                    if (!CountingMasters.checkValid(tipeHewan)){
                        invalid += 1;
                    } else{
                        continue;
                    }
                } else if (tipeAtraksi.equals("Dancing Animals")){
                    if (!DancingAnimals.checkValid(tipeHewan)){
                        invalid += 1;
                    } else{
                        continue;
                    }
                } else { // Passionate Coders
                    if (!PassionateCoders.checkValid(tipeHewan)){
                        invalid += 1;
                    } else{
                        continue;
                    }
                }
            } else{
                invalid += 1;
            }
        } return invalid;
    }
}