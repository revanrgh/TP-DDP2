package javari.reader;

import javari.animal.*;
import javari.park.*;
import javari.reader.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

public class AnimalRecords extends CsvReader{

    long validAnimal = 0;
    int counter = 0;

    public AnimalRecords(Path file) throws IOException{
        super(file);
    }
    
    //membuat method untuk memproses record-record dari file
    public void count(){
        for(int x = 0; x < lines.size(); x++){
            // Membagi input sesuai dengan
            String input = lines.get(x);
            String[] inputSplit = input.split(",");

            Integer inputID =  Integer.valueOf(inputSplit[0]);
            String inputAnimal = inputSplit[1];
            String inputName = inputSplit[2];
            Gender inputGender = Gender.parseGender(inputSplit[3]);
            double inputLength = Double.parseDouble(inputSplit[4]);
            double inputWeight = Double.parseDouble(inputSplit[5]);
            String specialStatus = inputSplit[6];
            Condition condition = Condition.parseCondition(inputSplit[7]);

            // Cek jika input termasuk ke dalam list hewan
            if (Arrays.asList(Mammals.getMamalia()).contains(inputAnimal)){
                Animal hewan = new Mammals(inputID, inputAnimal, inputName, inputGender,
                    inputLength, inputWeight, specialStatus, condition);
                validAnimal += 1;
            }else if (Arrays.asList(Aves.getAves()).contains(inputAnimal)){
                Animal hewan = new Aves(inputID, inputAnimal, inputName, inputGender,
                    inputLength, inputWeight, specialStatus, condition);
                validAnimal += 1;
            }else if (Arrays.asList(Reptiles.getReptilia()).contains(inputAnimal)){
                Animal hewan = new Reptiles(inputID, inputAnimal, inputName, inputGender,
                    inputLength, inputWeight, specialStatus, condition);
                validAnimal += 1;
            }
            //menambahkan animal baru ke dalam atraksi-atraksi dengan tipe yang sesuai
            for(int y = 0; y < Attractions.getListAttractions().size(); y++){
                if(Attractions.getListAttractions().get(y).getType().equalsIgnoreCase(inputAnimal)){
                    Attractions.getListAttractions().get(y).addPerformer(hewan);
                }
            }
        }
        counter += 1;
    }
    
    // Mengembalikan jumlah hewan yang validAnimal
    public long countValidRecords(){
        if(counter == 0){
            count();
        }
        return validAnimal;
    }

    // Mengembalikan jumlah hewan yang tidak validAnimal
    public long countInvalidRecords(){
        return lines.size() - countValidRecords();
    }
    
}