/** 
* @author : Revan Ragha Andhito
* NPM : 1706039420
*/

import java.awt.Dimension;
import javax.swing.JFrame;

/*
* A class represnting the memory game
* that called the main of other class
*/
public class MemoryGame{
    public static void main(String[] args){
        Board board = new Board();
        board.setPreferredSize(new Dimension(600,550));
        board.setLocation(300, 100);
        board.setResizable(false);
        board.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        board.pack();
        board.setVisible(true);
    }   
}