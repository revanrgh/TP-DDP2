/** 
* @author : Revan Ragha Andhito
* NPM : 1706039420
*/

import javax.swing.JButton;

/**
* A class representing a card and it's properties
* that extends GUI JButton
*/
public class Card extends JButton{
    
    /** Instance variables 
    int Id and boolean matched 
    of a card */
    private int id;
    private boolean matched = false;

    /**
    * Accessor for id card field. 
    * @return int of this card's Id
    */
    public int getId(){
        return this.id;
    }

    /**
    * Mutator for id field. 
    */
    public void setId(int id){
        this.id = id;
    }

    /**
    * Accessor for boolean matched
    * @return boolean which card that 
    * has matched or not matched
    */
    public boolean getMatched(){
        return this.matched;
    }

    /**
    * Mutator for card matched field. 
    */
    public void setMatched(boolean matched){
        this.matched = matched;
    }
    
}