/** 
* @author : Revan Ragha Andhito
* NPM : 1706039420
*/

/** Import API for this class and GUI needs */
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.event.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

/* A class represeting the board game
* that extends GUI JFrame.
* It contains properties, such as
* number of tries, restart and exit button
*/
public class Board extends JFrame{

    /** Instance variable that 
    supports the Board class*/
    private String imgSrc = "D:/REVAN/DOCUMENTS/UI/TP-DDP2/TP-DDP2/assignment-4/img/";
    private ArrayList<Card> cards;
    private ArrayList<Integer> numList;
    private ArrayList<Card> cardsList;
    private Card selectedCard;
    private Card firstCard;
    private Card secondCard;
    private Timer timer;
    private int counter;
    
    JLabel label;
    JPanel mainPanel;
    JPanel boardPanel;
    JPanel subPanel;
    JButton btRestart;
    JButton btExit;

    /** Constructor for Board Class */
    public Board(){

        cardsList = new ArrayList<Card>();
        numList = new ArrayList<Integer>();

        doRandom(); 

        // Make the card object based on id number
        for (int num : numList){
            Card newCard = new Card();
            newCard.setId(num);
            newCard.setIcon(new ImageIcon(imgSrc + "cover_icon.png"));
            newCard.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae){
                    selectedCard = newCard;
                    chooseCards();
                }
            });
            cardsList.add(newCard);
        }
        this.setCards(cardsList);

        //set up the timer if the 2 button clicked
        timer = new Timer(750, new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                checkCards();
            }
        });
        timer.setRepeats(false);

        // Make the board panel that contains 36 cards
        boardPanel = new JPanel();
        boardPanel.setLayout(new GridLayout(6, 6));
        boardPanel.setSize(40,40);
        for (Card card : this.cards){
            boardPanel.add(card);
        }

        // Make the restart button
        btRestart = new JButton("Restart");
        btRestart.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                restart();
            }
        });

        // Make the exit button
        btExit = new JButton("Exit");
        btExit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                exit();
            }
        });

        // Set up the sub panel for 
        // the exit and restart button
        // and the number of tries
        subPanel = new JPanel();
        subPanel.setLayout(new GridLayout(2, 1));
        subPanel.add(btRestart);
        subPanel.add(btExit);
        label = new JLabel("Number of tries : " + counter);
        label.setVerticalAlignment(SwingConstants.CENTER);
        subPanel.add(label);

        // Set up the panel into the frame
        mainPanel = new JPanel();
        mainPanel.add(boardPanel);
        mainPanel.add(subPanel);
        this.add(mainPanel);
        setTitle("Memory Game");
    }

    /*
    * Mutator to set the new Card List
    */
    public void setCards(ArrayList<Card> newCardList){
        this.cards = newCardList;
    }

    /*
    * Method to random the Id Card number
    */
    public void doRandom(){
        int pairs = 18;
        for (int i = 1; i < pairs+1; i++){
            numList.add(i);
            numList.add(i);
        }
        Collections.shuffle(numList);
    }

    /*
    * Method for the condition 
    * if user has clicked 2 buttons
    */
    public void chooseCards(){
        // If one button has clicked
        if (firstCard == null && secondCard == null){
            firstCard = selectedCard;
            firstCard.setIcon(new ImageIcon(imgSrc + "icon-" + firstCard.getId() + ".png"));
        }
        // If two button has clicked
        if (firstCard != null && firstCard != selectedCard && secondCard == null){
            secondCard = selectedCard;
            secondCard.setIcon(new ImageIcon(imgSrc + "icon-" + secondCard.getId() + ".png"));
            timer.start();
        }
    }

    /*
    * Method for check the card
    * is it matche with each other or not
    */
    public void checkCards(){
        // match condition
        if (firstCard.getId() == secondCard.getId()){
            firstCard.setEnabled(false); 
            secondCard.setEnabled(false);
            firstCard.setMatched(true); 
            secondCard.setMatched(true);
            this.counter += 1;
            label.setText("Number of tries : " + this.counter);
            // Condition all cards is opened
            if (this.isGameWon()){
                JOptionPane.showMessageDialog(this, "You win!\nNumber of tries : " + this.counter);
                playAgain();
            }
        }
        // unmatched contion
        else{
            firstCard.setEnabled(true); 
            secondCard.setEnabled(true);
            firstCard.setIcon(new ImageIcon(imgSrc + "cover_icon.png"));
            secondCard.setIcon(new ImageIcon(imgSrc + "cover_icon.png"));
            this.counter += 1;
            label.setText("Number of tries : " + this.counter);

        }
        firstCard = null; 
        secondCard = null;
    }

    /*
    * Method for checking all cards is matched
    * and has their own pair
    */
    public boolean isGameWon(){
        for(Card c: cards){
            if (c.getMatched() == false){
                return false;
            }
        }
        return true;
    }

    /* Method to do exit button */
    public void exit(){                                                       
        int option = JOptionPane.showConfirmDialog(this, "Are you sure?");
        
        switch(option){
            case JOptionPane.YES_OPTION: 
                System.exit(0);
                break;
            case JOptionPane.NO_OPTION:
                break;
            case JOptionPane.CANCEL_OPTION:
                break;
        }
    }

    /* Method to play again if user has won */
    public void playAgain(){                                                       
        int option = JOptionPane.showConfirmDialog(this, "Do you want to play again?");
        
        switch(option){
            case JOptionPane.YES_OPTION:
                firstCard.setMatched(false); 
                secondCard.setMatched(true);
                restart();
                break;
            case JOptionPane.NO_OPTION:
                System.exit(0);
                break;
            case JOptionPane.CANCEL_OPTION:
                break;
        }
    }
    
    /* Method to do restart button */
    public void restart(){
        firstCard = null;
        secondCard = null;
        doRandom();
        int index = 0;
        for (Card card : cards){
            card.setId(numList.get(index));
            card.setEnabled(true);
            card.setIcon(new ImageIcon(imgSrc + "cover_icon.png"));
            index += 1;
        }
        this.counter = 0;
        label.setText("Number of tries : " + this.counter);
    }

}