/**
* ASSIGNMENT 1
* Author        : Revan Ragha Andhito
* NPM           : 1706039420
* Kelas         : DDP 2 - D
*/

public class WildCat {

    String name;
    double weight; // In kilograms
    double length; // In centimeters

    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    public double computeMassIndex() {
        double bmi = (weight)/((length/100)*(length/100));
        return bmi;
    }
}
