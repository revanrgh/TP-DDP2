/**
* ASSIGNMENT 1
* Author		: Revan Ragha Andhito
* NPM			: 1706039420
* Kelas			: DDP 2 - D
*/

import java.util.Scanner;
import java.util.ArrayList;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms
    private static int countCat = 0;

    // Method depart untuk mencetak output dan memanggil Class TrainCar
    public static void depart(TrainCar train, int countCat){
    	System.out.println("The train departs to Javari Park");
    	System.out.print("[LOCO]<");
    	train.printCar();    	

	    String bmiType = "";
	    double meanWeight = train.computeTotalMassIndex()/countCat;

	    if (meanWeight < 18.5){
	    	bmiType = "underweight";}
	    else if (meanWeight >= 18.5 && meanWeight < 25){
			bmiType = "normal";}
	    else if (meanWeight >= 25 && meanWeight < 30){
	   		bmiType = "overweight";}
	    else if (meanWeight >= 18.5){
	   		bmiType = "obese";}

	    System.out.printf("\nAverage mass index of all cats: %.2f", meanWeight);
	    System.out.println("\nIn average, the cats in the train are *"+ bmiType + "*");
    }

    public static void main(String[] args) {
 		Scanner input = new Scanner(System.in);
    	int intInput = Integer.parseInt(input.nextLine());
    	TrainCar train = null;
    	// ArrayList untuk membuat rangkaian jumlah kereta dan kucing yang dimasukkan
    	ArrayList<Integer> jumlahKucing = new ArrayList<>();
    	ArrayList<TrainCar> jumlahKereta = new ArrayList<>();

    	for (int a = 0; a < intInput; a++) {
    		countCat += 1;
    		String strInput = input.nextLine();
    		String[] splitStr = strInput.split(",");

    		double dbWeight = Double.parseDouble(splitStr[1]); 
    		double dbLength = Double.parseDouble(splitStr[2]);

    		WildCat cat = new WildCat(splitStr[0], dbWeight, dbLength);
    		train = new TrainCar(cat,train);

    		if (train.computeTotalWeight() >= THRESHOLD){
    			jumlahKucing.add(countCat);
    			jumlahKereta.add(train);

     			countCat = 0;
    			train = null;}
    	}

    	if (train != null){
    		jumlahKucing.add(countCat);
    		jumlahKereta.add(train);}

    	for (int i = 0; i < jumlahKereta.size(); i++){
    		depart(jumlahKereta.get(i),jumlahKucing.get(i));}
    	System.exit(0);

    }
}